﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class HandAnimator : MonoBehaviour
{
    public float speed = 5.0f;
    public float animationSecond = 5;
    public VRTK_ControllerEvents controller = null;
    private bool check = false;

    private Animator animator = null;
    private readonly List<Finger> gripFingers = new List<Finger>()
    {
        new Finger(FingerType.Middle),
        new Finger(FingerType.Ring),
        new Finger(FingerType.Pinky)
    };

    private readonly List<Finger> pointFingers = new List<Finger>()
    {
        new Finger(FingerType.Index),
        new Finger(FingerType.Thumb)
    };

    // Start is called before the first frame update
    void Awake()
    {
        animator = GetComponent<Animator>();        
    }

    // Update is called once per frame
    void Update()
    {
        //if (transform.hasChanged)
        //{
        //Store input data
        //CheckGrip();
        //CheckPointer();

        ////Smooth input values
        //SmoothFinger(pointFingers);
        //SmoothFinger(gripFingers);

        ////Apply smoothed values
        //AnimateFinger(pointFingers);
        //AnimateFinger(gripFingers);

        //    transform.hasChanged = false;
        //}



        CheckGrip();
        SmoothFinger(gripFingers);

        CheckPointer();
        SmoothFinger(pointFingers);


    }

   

    private void CheckGrip() {

        if (controller.gripAxisChanged)
        {
            float gripValue = controller.GetGripAxis();
            SetFingerTargets(gripFingers, gripValue);
            
        }
    }

    private void CheckPointer()
    {
        if (controller.triggerAxisChanged)
        {
            float pointValue = controller.GetTriggerAxis();
            SetFingerTargets(pointFingers, pointValue);
        }



    }

    private void SetFingerTargets(List<Finger> fingers, float value)
    {
        foreach (Finger finger in fingers)
        {
            finger.target = value;
        }
    }

    private void SmoothFinger(List<Finger> fingers)
    {
        foreach (Finger finger in fingers)
        {
            float time = speed * Time.unscaledDeltaTime;
            finger.current = Mathf.MoveTowards(finger.current, finger.target, time);

            //No need to loop two times
            animator.SetFloat(finger.type.ToString(), finger.current);
        }


    }

    private void AnimateFinger(List<Finger> fingers)
    {
        foreach (Finger finger in fingers)
        {
            AnimateFinger(finger.type.ToString(), finger.current);
        }
    }

    private void AnimateFinger(string finger, float blend)
    {
        animator.SetFloat(finger, blend);
    }


}
