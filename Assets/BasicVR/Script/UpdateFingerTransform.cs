﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateFingerTransform : MonoBehaviour
{
    public Animator animatorHand;

    public void SetAnimatorChangeState(string state, float value)
    {
        if(animatorHand != null)
        {
            animatorHand.SetFloat(state, value);
        }
       
    }


    public void SetAnimatorLayerWeight(int layer, float value)
    {
        if (animatorHand != null)
        {
            animatorHand.SetLayerWeight(layer, value);
        }
            
    }

}
