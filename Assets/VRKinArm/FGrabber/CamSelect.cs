﻿using UnityEngine;
using System.Collections;

public class CamSelect : MonoBehaviour {

    private GameObject plane;

    WebCamTexture webcamtexture;
    WebCamDevice[] devices;
    int indDevice = 0;
	public  int resolutionDivisor ;
	public int deviceID;

	// Use this for initialization
	void Start () {
		indDevice = deviceID;
        plane = new GameObject();
        plane = this.transform.parent.gameObject;

        devices = WebCamTexture.devices;


        GetComponent<TextMesh>().text = (indDevice +1 )+ "/" + devices.Length + " " + devices[indDevice].name;

		webcamtexture = new WebCamTexture(WebCamTexture.devices[indDevice].name,1920 / resolutionDivisor, 1080/resolutionDivisor, 60);

        plane.GetComponent<Renderer>().material.mainTexture = webcamtexture;
        webcamtexture.Play();
        plane.GetComponent<Renderer>().material.shader = Shader.Find("Unlit/Texture");
        plane.GetComponent<Renderer>().material.SetTextureOffset("_MainTex", new Vector2(1, 1));
        plane.GetComponent<Renderer>().material.SetTextureScale("_MainTex", new Vector2(-1, -1));

    }
	


	// Update is called once per frame
	void Update () {
        

    }
}
