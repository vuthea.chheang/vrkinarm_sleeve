using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;
using Tobii.XR;
using Tobii.G2OM;

public class DrawHeatmap : MonoBehaviour
{

    public bool _useEyeForHeatmap;
    public DeviceType deviceType;

    public enum DeviceType { GazeHeadset, EyeTracking}

    public bool UseLiveInputStream = false;

    public Texture2D HeatmapLookUpTable;

    [SerializeField]
    private float drawBrushSize = 2000.0f; // aka spread

    [SerializeField]
    private float drawIntensity = 15.0f; // aka amplitude

    [SerializeField]
    private float minThreshDeltaHeatMap = 0.001f; // Mostly for performance to reduce spreading heatmap for small values.

    private Texture2D myDrawTex;

    public Material HeatmapOverlayMaterialTemplate;
    private int _matIndex;
    private Material[] mats2;

    //Eye focus
    private bool _hasFocus;

    //Using for reset heatmap visualization
    private bool neverDrawnOn = true;

    //Eye data
    private TobiiXR_EyeTrackingData eyeTrackingData;
    private RaycastHit hit;
    public Vector3 rayOrigin { get; private set; }
    public Vector3 rayDirection { get; private set; }

    [Header("Target Renderer")]
    [SerializeField]
    private Renderer myRenderer;

    [Header("Gaze Visualizer")]
    [SerializeField]
    private GameObject gazeVisualizer;

    //Layer mask for heatmap
    [SerializeField]
    private LayerMask layer_mask;

    [Header("VR Heatset")]
    private DateTime _timerStart;
    //private bool _startLog;
    [SerializeField]
    private Transform _headsetTransform;
    public Vector2 _eyeHitPos { get; private set; }


    [Header("Scenario")]
    private string _scenarioName = "KinArmVR";

    //private LoggingData _logData;

    public void ResetHeatmap()
    {
        myDrawTex = Instantiate(HeatmapOverlayMaterialTemplate.mainTexture) as Texture2D;
        mats2[_matIndex].mainTexture = myDrawTex;

        MyDrawTexture = myDrawTex;
    }

    public void EnableHeatmap()
    {
        _useEyeForHeatmap = true;
        UseLiveInputStream = true;
    }

    public void ResetHeatmapOnDisable()
    {
        _useEyeForHeatmap = false;
        UseLiveInputStream = false;
        //  StartCoroutine(WaitToClearHeatmap());
        ResetHeatmap();
    }

    IEnumerator WaitToClearHeatmap()
    {
        if (MyDrawTexture != null)
        {
            for (int ix = 0; ix < MyDrawTexture.width; ix++)
            {
                for (int iy = 0; iy < MyDrawTexture.height; iy++)
                {
                    MyDrawTexture.SetPixel((int)(ix), (int)(iy), new Color(0, 0, 0, 0));
                }
            }
            neverDrawnOn = false;

            Debug.Log("clear texture: " + neverDrawnOn);
        }
        else
        {
            Debug.Log("note sure  texture");
        }
        // Assign colors
        yield return null;
        MyDrawTexture.Apply();

    }


    //public void CreateNewLog()
    //{
    //    _startLog = true;
    //    _timerStart = DateTime.UtcNow;
    //}

    //public void StopLogging()
    //{
    //    _startLog = false;
    //}

    private void Awake()
    {
        SetDrawTexture();
        //StartCoroutine(WaitToClearHeatmap());
    }

    private void Start()
    {
        //_controller = ScenarioController._instance;

        //_headsetTransform = VRTK_DeviceFinder.HeadsetCamera();

        eyeTrackingData = TobiiXR.GetEyeTrackingData(TobiiXR_TrackingSpace.World);

        if(layer_mask == 0)
            layer_mask = LayerMask.GetMask("Heatmap");

        //_logData = new LoggingData();

        //_startLog = LoggingSystem.IsLogging;
    }


    private void Update()
    {

        if (_useEyeForHeatmap)
        {
            if (deviceType == DeviceType.EyeTracking)
            {
                //if (_headsetTransform == null)
                //{
                //    //_headsetTransform = VRTK_DeviceFinder.HeadsetCamera();
                //    return;
                //}

                // Check if gaze ray is valid
                if (eyeTrackingData.GazeRay.IsValid)
                {


                    // The origin of the gaze ray is a 3D point
                    rayOrigin = eyeTrackingData.GazeRay.Origin;

                    // The direction of the gaze ray is a normalized direction vector
                    rayDirection = eyeTrackingData.GazeRay.Direction;

                    if (Physics.Raycast(rayOrigin, rayDirection, out hit, 50f, layer_mask))
                    {
                        _eyeHitPos = hit.textureCoord;

                        //Debug.Log(_eyeHitPos);

                        if (UseLiveInputStream)
                            OnLookAt(_eyeHitPos);
                        //OnLookAtSphere(_eyeHitPos);

                        //Write data to log
                        //if (_startLog)
                        //    WriteLoggedData();

                    }
                }


            }
            else if (deviceType == DeviceType.GazeHeadset)
            {
                if (Time.frameCount % 5 == 0)
                {
                    rayOrigin = _headsetTransform.position;
                    rayDirection = _headsetTransform.forward;

                    if (Physics.Raycast(rayOrigin, rayDirection, out hit, 50f, layer_mask))
                    {
                        _eyeHitPos = hit.textureCoord;

                        //Debug.Log(_eyeHitPos);

                        if (UseLiveInputStream)
                            OnLookAt(_eyeHitPos);
                        //OnLookAtSphere(_eyeHitPos);
                    }
                }
            }
        }          
        
    }

    public void EnableGazeVisualizer(bool isEnable)
    {
        gazeVisualizer.SetActive(isEnable);
    }


    private void WriteLoggedData()
    {

        //_logData.scenario = _scenarioName;
        //_logData.timeStamp = eyeTrackingData.Timestamp;
        //_logData.DtInMs = (DateTime.UtcNow - _timerStart).TotalMilliseconds;
        //_logData.headPosition = _headsetTransform.position;
        //_logData.headRotation = _headsetTransform.rotation.eulerAngles;
        //_logData.eyeOrigin = rayOrigin;
        //_logData.eyeDirection = rayDirection;
        //_logData.eyeHitPosUV = _eyeHitPos;


        //LoggingSystem.writeLoggingData(_logData);
    }

    public void OnLookAtSphere(Vector2 posUV)
    {
        StartCoroutine(DrawAt(posUV));
    }

    public void OnLookAt(Vector3 pos)
    {
        if (UseLiveInputStream)
        {
            DrawAtThisHitPos(pos);
        }
    }

    public void DrawAtThisHitPos(Vector3 hitPosition)
    {
        StartCoroutine(DrawAt(hitPosition));

        //Vector2? hitPosUV = hitPosition;
        ////Vector2? hitPosUV = GetCursorPosInTexture(hitPosition);
        ////Vector2? hitPosUV = GetCursorPosInTextureSphere(hitPosition, MyDrawTexture.width, MyDrawTexture.height);
        //if (hitPosUV != null)
        //{
        //    // DrawAt(hitPosUV.Value, Color.green);
        //    StartCoroutine(DrawAt(hitPosUV.Value));
        //}
    }

    private void DrawAt(Vector2 posUV, Color col)
    {
        if (MyDrawTexture != null)
        {
            Debug.LogFormat("New draw point at ( {0}; {1} )", posUV.x, posUV.y);
            MyDrawTexture.SetPixel(
                (int)(posUV.x * MyDrawTexture.width),
                (int)(posUV.y * MyDrawTexture.height),
                col);

            MyDrawTexture.Apply();
        }
    }

    Vector2 prevPos = new Vector2(-1, -1);
    float dynamicRadius = 0;

    private void DrawAt2(Vector2 posUV, int maxRadius, float intensity)
    {
        if (MyDrawTexture != null)
        {
            // Reset on first draw
            if (neverDrawnOn)
            {
                for (int ix = 0; ix < MyDrawTexture.width; ix++)
                {
                    for (int iy = 0; iy < MyDrawTexture.height; iy++)
                    {
                        MyDrawTexture.SetPixel((int)(ix), (int)(iy), new Color(0, 0, 0, 0));
                    }
                }
                neverDrawnOn = false;
            }

            // Determine the center of our to be drawn 'blob'
            Vector2 center = new Vector2(posUV.x * MyDrawTexture.width, posUV.y * MyDrawTexture.height);

            // Determine appropriate radius based on viewing duration: The longer looking in a small region the larger the radius
            float dist = maxRadius;
            for (int ix = -(int)(dynamicRadius / 2); ix < dynamicRadius / 2; ix++)
            {
                for (int iy = -(int)(dynamicRadius / 2); iy < dynamicRadius / 2; iy++)
                {
                    float tx = posUV.x * MyDrawTexture.width + ix;
                    float ty = posUV.y * MyDrawTexture.height + iy;

                    Vector2 currPnt = new Vector2(tx, ty);

                    float distCenterToCurrPnt = Vector2.Distance(center, currPnt);

                    if (distCenterToCurrPnt <= dynamicRadius / 2)
                    {
                        float normalizedDist = (distCenterToCurrPnt / dynamicRadius / 2); // [0.0, 1.0]
                        float B = 4f;
                        float localNormalizedInterest = Mathf.Clamp(1 / (1 + Mathf.Pow(Mathf.Epsilon, -(B * normalizedDist))), 0, 1);

                        Color baseColor = MyDrawTexture.GetPixel((int)tx, (int)ty);
                        float delta = intensity * (1 - Mathf.Abs(localNormalizedInterest));

                        float normalizedInterest = baseColor.a + delta;
                        Color col = Color.red;
                        // Get color from  given heatmap ramp
                        if (HeatmapLookUpTable != null)
                        {
                            col = HeatmapLookUpTable.GetPixel((int)(normalizedInterest * HeatmapLookUpTable.width), 0);
                            col = new Color(col.r, col.g, col.b, normalizedInterest);
                        }
                        else
                            col = new Color(col.r, col.g, col.b, normalizedInterest);


                        MyDrawTexture.SetPixel((int)(tx), (int)(ty), col);

                        Color baseColor2 = MyDrawTexture.GetPixel((int)tx, (int)ty);
                    }
                }
            }

            MyDrawTexture.Apply();
        }
    }

    private IEnumerator DrawAt(Vector2 posUV)
    {
        if (MyDrawTexture != null)
        {
            // Reset on first draw ----> Cost much more performance
            //if (neverDrawnOn)
            //{
            //    for (int ix = 0; ix < MyDrawTexture.width; ix++)
            //    {
            //        for (int iy = 0; iy < MyDrawTexture.height; iy++)
            //        {
            //            MyDrawTexture.SetPixel((int)(ix), (int)(iy), new Color(0, 0, 0, 0));
            //        }
            //    }
            //    neverDrawnOn = false;
            //}

            // Assign colors
            yield return null;

            StartCoroutine(ComputeHeatmapAt(posUV, true, true));
            yield return null;

            StartCoroutine(ComputeHeatmapAt(posUV, true, false));
            yield return null;

            StartCoroutine(ComputeHeatmapAt(posUV, false, true));
            yield return null;

            StartCoroutine(ComputeHeatmapAt(posUV, false, false));
            yield return null;

            MyDrawTexture.Apply();
        }
    }

    private IEnumerator ComputeHeatmapAt(Vector2 currPosUV, bool positiveX, bool positiveY)
    {
        yield return null;

        // Determine the center of our to be drawn 'blob'
        Vector2 center = new Vector2(currPosUV.x * MyDrawTexture.width, currPosUV.y * MyDrawTexture.height);
        int sign_x = (positiveX) ? 1 : -1;
        int sign_y = (positiveY) ? 1 : -1;
        int start_x = (positiveX) ? 0 : 1;
        int start_y = (positiveY) ? 0 : 1;

        for (int dx = start_x; dx < MyDrawTexture.width; dx++)
        {
            float tx = currPosUV.x * MyDrawTexture.width + dx * sign_x;
            if ((tx < 0) || (tx >= MyDrawTexture.width))
                break;

            for (int dy = start_y; dy < MyDrawTexture.height; dy++)
            {
                float ty = currPosUV.y * MyDrawTexture.height + dy * sign_y;
                if ((ty < 0) || (ty >= MyDrawTexture.height))
                    break;

                Color? newColor = null;
                if (ComputeHeatmapColorAt(new Vector2(tx, ty), center, out newColor))
                {
                    if (newColor.HasValue)
                    {
                        MyDrawTexture.SetPixel((int)(tx), (int)(ty), newColor.Value);
                    }
                }
                else
                {
                    break;
                }
            }
        }
    }

    private bool ComputeHeatmapColorAt(Vector2 currPnt, Vector2 origPivot, out Color? col)
    {
        col = null;


        float spread = drawBrushSize;
        float amplitude = drawIntensity;
        float distCenterToCurrPnt = Vector2.Distance(origPivot, currPnt) / spread;

        float B = 2f;
        float scaledInterest = 1 / (1 + Mathf.Pow(Mathf.Epsilon, -(B * distCenterToCurrPnt)));
        float delta = scaledInterest / amplitude;
        if (delta < minThreshDeltaHeatMap)
            return false;

        Color baseColor = MyDrawTexture.GetPixel((int)currPnt.x, (int)currPnt.y);
        float normalizedInterest = Mathf.Clamp(baseColor.a + delta, 0, 1);

        // Get color from given heatmap ramp
        if (HeatmapLookUpTable != null)
        {
            col = HeatmapLookUpTable.GetPixel((int)(normalizedInterest * (HeatmapLookUpTable.width - 1)), 0);
            col = new Color(col.Value.r, col.Value.g, col.Value.b, normalizedInterest);
        }
        else
        {
            col = Color.blue;
            col = new Color(col.Value.r, col.Value.g, col.Value.b, normalizedInterest);
        }

        return true;
    }

    private Renderer MyRenderer
    {
        get
        {
            if (myRenderer == null)
            {
                myRenderer = GetComponent<Renderer>();
            }
            return myRenderer;
        }
    }

    private Texture2D MyDrawTexture
    {
        get; set;
    }

    public void SetDrawTexture()
    {
        if (myDrawTex == null)
        {
            if (MyRenderer == null)
            {
                return;
            }

            Material[] mats = MyRenderer.sharedMaterials;

            _matIndex = mats.Length;
            mats2 = new Material[_matIndex + 1];
            for (int i = 0; i < mats.Length; i++)
            {
                mats2[i] = mats[i];
            }
            mats2[_matIndex] = Instantiate(HeatmapOverlayMaterialTemplate) as Material;
            MyRenderer.sharedMaterials = mats2;

            // We don't want to overwrite the original texture. 
            // By creating an instance, we're not changing the texture itself.
            // Thus, when restarting the app, everything is set back to how it was.
            if (mats2 == null)
            {
                Debug.Log("mats2 -> null");
            }
            else if (mats2[_matIndex] == null)
            {
                Debug.Log("mats2.length -> null");
            }
            else if (mats2[_matIndex].mainTexture == null)
            {
                Debug.Log("mats2.maintex -> null");
            }
            else
            {
                myDrawTex = Instantiate(mats2[_matIndex].mainTexture) as Texture2D;
                mats2[_matIndex].mainTexture = myDrawTex;
            }

            MyDrawTexture = myDrawTex;
        }
    }

    /// <summary>
    /// Determine the position of the cursor within the texture in UV space.
    /// </summary>
    /// <returns>True if this GameObject is hit.</returns>
    private Vector2? GetCursorPosInTexture(Vector3 hitPosition)
    {
        Vector2? hitPointUV = null;

        try
        {
            Vector3 center = gameObject.transform.position;
            Vector3 halfsize = gameObject.transform.localScale / 2;

            // Let's transform back to the origin: Translate & Rotate
            Vector3 transfHitPnt = hitPosition - center;

            // Rotate around the y axis
            // transfHitPnt = Quaternion.AngleAxis(-(this.gameObject.transform.rotation.eulerAngles.y - 180), Vector3.up) * transfHitPnt;
            transfHitPnt = Quaternion.AngleAxis(-(this.gameObject.transform.rotation.eulerAngles.y - 180), Vector3.up) * transfHitPnt;
            //transfHitPnt = Quaternion.AngleAxis(gameObject.transform.rotation);
            // Rotate around the x axis
            transfHitPnt = Quaternion.AngleAxis(this.gameObject.transform.rotation.eulerAngles.x, Vector3.right) * transfHitPnt;

            // Normalize the transformed hit point to as UV coordinates are in [0,1].
            float uvx = (Mathf.Clamp(transfHitPnt.x, -halfsize.x, halfsize.x) + halfsize.x) / (2 * halfsize.x);
            float uvy = (Mathf.Clamp(transfHitPnt.y, -halfsize.y, halfsize.y) + halfsize.y) / (2 * halfsize.y);
            hitPointUV = new Vector2(uvx, uvy);
        }
        catch (UnityEngine.Assertions.AssertionException)
        {
            Debug.LogError(">> AssertionException");
        }

        return hitPointUV;
    }

    ////The method of the "IGazeFocusable" interface, which will be called when this object receives or loses focus
    public void GazeFocusChanged(bool hasFocus)
    {
        //This object either received or lost focused this frame, as indicated by the hasFocus parameter.
        //if (hasFocus)
        //{
        //    Debug.Log(hasFocus);
        //}

        _hasFocus = hasFocus;
    }
}
