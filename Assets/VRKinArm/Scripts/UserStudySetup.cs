﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UserStudySetup : MonoBehaviour
{

    [Header("UI Controller")]
    [SerializeField]
    private TMP_InputField participantId;

    [SerializeField]
    private GameObject scenariosPanel;

    [SerializeField]
    private TMP_Dropdown conditionDropDown;

    [SerializeField]
    private TMP_Dropdown taskDropdown;
    [SerializeField]
    private TMP_Dropdown directionDropdown;
    [SerializeField]
    private Toggle isLiveHeatmap;


    [Header("Task Manager")]
    [SerializeField]
    private TaskMananger taskManager;
    [SerializeField]
    private LoggingSystem logging;
    [SerializeField]
    private DrawHeatmap heatmap;

    public void StartingTask()
    {
        if (logging)
        {
            logging.userId = int.Parse(participantId.text);
            logging.CreateLog();
            logging.ActivateLog();
        }

        if (heatmap)
        {
            heatmap.UseLiveInputStream = isLiveHeatmap.isOn;
        }


        if (taskManager)
        {
            scenariosPanel.SetActive(false);
            taskManager.ActivatingTask(conditionDropDown.value, taskDropdown.value, directionDropdown.value);
        }
    }

    public void EnableScenarioPanel(bool flag)
    {
        scenariosPanel.SetActive(flag);
        taskManager.ResetTasks();
    }

    public void ResetingTask()
    {

    }

}
