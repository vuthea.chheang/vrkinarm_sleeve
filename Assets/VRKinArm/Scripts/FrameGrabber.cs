﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FrameGrabber : MonoBehaviour
{
    [Header("Resolution")]
    [SerializeField]
    int screenWidth = 1920;
    [SerializeField]
    int screenHeight = 1080;
    [SerializeField]
    int targetScreenWidth = 960;
    [SerializeField]
    int targetScreenHeight = 1080;
    [SerializeField]
    int targetX = 0;
    [SerializeField]
    int targetY = 0;
    [SerializeField]
    int frameCount = 3;

    WebCamTexture webcamtexture;
    public int resolutionDivisor = 1;

    [SerializeField]
    private Renderer grabberScreen;

    bool isCapturingStarted = false;
    Texture2D croppedTexture;

    private void Start()
    {
        if (!grabberScreen)
            grabberScreen = GetComponent<Renderer>();

        croppedTexture = new Texture2D(targetScreenWidth, targetScreenHeight);
    }


    private void Update()
    {

        if(Time.frameCount % frameCount == 0)
        {
            if (isCapturingStarted)
            {
                //**CPU Processing **/
                //croppedTexture.SetPixels(webcamtexture.GetPixels(targetX, targetY, targetScreenWidth, targetScreenHeight));
                //croppedTexture.Apply();

                //**GPU Processing **/
                Graphics.CopyTexture(webcamtexture, 0, 0, targetX, targetY, targetScreenWidth, targetScreenHeight, croppedTexture, 0, 0, 0, 0);

                grabberScreen.material.mainTexture = croppedTexture;
            }
        }        
    }

    public void StartCapturing(int deviceID)
    {
        webcamtexture = new WebCamTexture(WebCamTexture.devices[deviceID].name, screenWidth / resolutionDivisor, screenHeight / resolutionDivisor, 60);
        webcamtexture.mipMapBias = -0.5f;

        //grabberScreen.material.mainTexture = targetTexture;
        webcamtexture.Play();

        isCapturingStarted = true;
    }


}
