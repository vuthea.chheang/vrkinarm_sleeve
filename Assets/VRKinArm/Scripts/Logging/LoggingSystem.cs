﻿using UnityEngine;
using System.IO;
using System.Text;

public class LoggingSystem : MonoBehaviour {

	#region FIELDS

	// static log file names and formatters
	public static string LOGFILE_DIRECTORY = Directory.GetCurrentDirectory() + "/Study_files";
    private string LOGFILE_DIRECTORY_USER = "User_";
	private string LOGFILE_NAME_BASE = "_log.csv";
	private string LOGFILE_NAME_TIME_FORMAT = "yyyy-MM-dd_HH-mm-ss"; // prefix of the logfile, created when application starts (year - month - day - hour - minute - second)

    // logfile reference of the current session
    public static string logFolder
    {
        get; private set;
    }
	public static string logFile;

	// bool representing whether the logging system should be used or not (set in the Unity Inspector)
	public bool logging = true;

    //file ID
    public int userId;
    public static int participantID = 0;

    //write header
    private static bool writeheader = false;

	#endregion

    public static bool IsLogging
    {
        get;
        set;
    }

    private static TextWriter textWriter;
    private static bool _textEditable;
    private static StringBuilder _builder;

	#region START_UPDATE

	/// <summary>
	/// Start this instance.
	/// </summary>
	public void CreateLog () //Awake
    { 

        if (logging)
        {
            IsLogging = logging;

            // check if directory exists (and create it if not)
            if (!Directory.Exists(LOGFILE_DIRECTORY)) Directory.CreateDirectory(LOGFILE_DIRECTORY);

            //participantID = Directory.GetFiles(LOGFILE_DIRECTORY, "*.csv", SearchOption.TopDirectoryOnly).Length + 1;
            if (userId == 0)
                participantID = Directory.GetDirectories(LOGFILE_DIRECTORY, LOGFILE_DIRECTORY_USER + "*", SearchOption.TopDirectoryOnly).Length + 1;
            else
                participantID = userId;

            logFolder = LOGFILE_DIRECTORY + "/" + LOGFILE_DIRECTORY_USER + participantID;

            if (!Directory.Exists(logFolder)) Directory.CreateDirectory(logFolder);

            // create file for this session using time prefix based on standard UTC time
            logFile = logFolder
                + "/"
                + LOGFILE_DIRECTORY_USER + participantID + "_"
                //+ System.DateTime.UtcNow.ToString(LOGFILE_NAME_TIME_FORMAT)
                //+ System.DateTime.UtcNow.AddHours(2.0).ToString(LOGFILE_NAME_TIME_FORMAT)	// manually adjust time zone, e.g. + 2 UTC hours for summer time in location Stockholm/Sweden
                + LOGFILE_NAME_BASE;


            if (File.Exists(logFile)) {
                writeheader = true;
                Debug.Log("[LoggingSystem] LogFile created at " + logFile); 
            
            }
            else
            {
                File.Create(logFile).Dispose();
                // Debug.LogError("[LoggingSystem] Error creating LogFile");
            }
        }
    }

    public void ActivateLog()
    {
        if (File.Exists(logFile) && !_textEditable)
        {
            textWriter = new StreamWriter(logFile, true);
            _textEditable = true;
        }
    }

    private void Start()
    {
        if (File.Exists(logFile))
        {
            textWriter = new StreamWriter(logFile, true);
            _textEditable = true;
        }
    }

    private void OnDestroy()
    {
        if(_textEditable)
            textWriter.Close();
    }

    #endregion


    #region WRITE_TO_LOG

    /// <summary>
    /// Writes the message to the log file on disk.
    /// </summary>
    /// <param name="message">string representing the message to be written.</param>
    //   private static void writeMessageToLog(string message)
    //{
    //       if (File.Exists(logFile))
    //       {
    //           TextWriter tw = new StreamWriter(logFile, true);
    //           tw.WriteLine(message);
    //           tw.Close();
    //       }
    //   }



    private static void writeMessageToLog(string message)
    {
        if (_textEditable)
        {
            textWriter.WriteLine(message);
        }


    }

    /// <summary>
    /// Writes the message including timestamp to the log file on disk.
    /// </summary>
    /// <param name="message">string representing the message to be written.</param>
    public void writeMessageWithTimestampToLog(string message)
	{
		writeMessageToLog(Time.realtimeSinceStartup.ToString() + ";" + message);
	}

    public static void writeLoggingData(LoggingData logged)
    {
        if (!writeheader)
        {
            _builder = new StringBuilder();

            _builder.AppendFormat("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33}",
                // UserId
                "UserId",
                // Condition
                "Condition",
                // Scenario
                "Scenario",
                //Direction
                "Direction",
                // Timestamp
                "TimeStamp",
                // Duration in ms
                "DtInMs",
                // Controller 
                "ConWPosX",
                "ConWPosY",
                "ConWPosZ",
                "ConPosX",
                "ConPosY",
                "ConPosZ",
                "ConRotX", 
                "ConRotY",
                "ConRotZ",
                // Hit point
                "IsHit",
                "DotId",
                "HitPosX",
                "HitPosY",
                "HitPosZ",
                // Head tracking
                "HeadPosX",
                "HeadPosY",
                "HeadPosZ",
                "HeadRotX",
                "HeadRotY",
                "HeadRotZ",
                // Eye tracking
                "EyeOriginX",
                "EyeOriginY",
                "EyeOriginZ",
                "EyeDirectionX",
                "EyeDirectionY",
                "EyeDirectionZ",
                // Eye hit pose coordinate
                "EyeHitPosX",
                "EyeHitPosY"
                );
            

            writeMessageToLog(_builder.ToString());
            writeheader = true;
        }

        _builder = new StringBuilder();

        _builder.AppendFormat("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},{24},{25},{26},{27},{28},{29},{30},{31},{32},{33}",
            logged.UserID,
            logged.Condition,
            logged.Scenario,
            logged.Direction,
            logged.TimeStamp,
            logged.DtInMs,
            logged.ControllerWorldPosition.x,
            logged.ControllerWorldPosition.y,
            logged.ControllerWorldPosition.z,
            logged.ControllerPosition.x,
            logged.ControllerPosition.y,
            logged.ControllerPosition.z,
            logged.ControllerRotation.x,
            logged.ControllerRotation.y,
            logged.ControllerRotation.z,
            logged.IsHit,
            logged.DotId,
            logged.HitPosition.x,
            logged.HitPosition.y,
            logged.HitPosition.z,
            logged.HeadPosition.x,
            logged.HeadPosition.y,
            logged.HeadPosition.z,
            logged.HeadRotation.x,
            logged.HeadRotation.y,
            logged.HeadRotation.z,
            logged.EyeOrigin.x,
            logged.EyeOrigin.y,
            logged.EyeOrigin.z,
            logged.EyeDirection.x,
            logged.EyeDirection.y,
            logged.EyeDirection.z,
            logged.EyeHitPoint.x,
            logged.EyeHitPoint.y
            );

        //write to textfile
        if(_textEditable)
            textWriter.WriteLine(_builder.ToString());
    }

    public static void writeLoggingDataSleeve(LoggingData logged)
    {
        if (!writeheader)
        {
            _builder = new StringBuilder();

            _builder.AppendFormat("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21}",
                // UserId
                "UserId",
                // Condition
                "Condition",
                // Scenario
                "Scenario",
                //Direction
                "Direction",
                // Timestamp
                "TimeStamp",
                // Duration in ms
                "DtInMs",
                // Controller 
                "ConWPosX",
                "ConWPosY",
                "ConWPosZ",
                "ConPosX",
                "ConPosY",
                "ConPosZ",
                "ConRotX",
                "ConRotY",
                "ConRotZ",
                // Hit point
                "IsHit",
                "DotId",
                "HitPosX",
                "HitPosY",
                "HitPosZ",
                // Resistance Change
                "Resistance",
                "TaskRound"

                );


            writeMessageToLog(_builder.ToString());
            writeheader = true;
        }

        _builder = new StringBuilder();

        _builder.AppendFormat("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21}",
            logged.UserID,
            logged.Condition,
            logged.Scenario,
            logged.Direction,
            logged.TimeStamp,
            logged.DtInMs,
            logged.ControllerWorldPosition.x,
            logged.ControllerWorldPosition.y,
            logged.ControllerWorldPosition.z,
            logged.ControllerPosition.x,
            logged.ControllerPosition.y,
            logged.ControllerPosition.z,
            logged.ControllerRotation.x,
            logged.ControllerRotation.y,
            logged.ControllerRotation.z,
            logged.IsHit,
            logged.DotId,
            logged.HitPosition.x,
            logged.HitPosition.y,
            logged.HitPosition.z,
            logged.ResistanceChange,
            logged.TaskRound
            );

        //write to textfile
        if (_textEditable)
            textWriter.WriteLine(_builder.ToString());
    }


    #endregion
}