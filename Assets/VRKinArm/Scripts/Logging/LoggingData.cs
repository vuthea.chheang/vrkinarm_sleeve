﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoggingData
{
    public int UserID { get; set; }
    public string Condition { get; set; }
    public string Scenario { get; set; }
    public string Direction { get; set; }
    public string TimeStamp { get; set; }
    public double DtInMs { get; set; }
    public Vector3 ControllerWorldPosition { get; set; }
    public Vector3 ControllerPosition { get; set; }
    public Vector3 ControllerRotation { get; set; }
    public bool IsHit { get; set; }
    public int DotId { get; set; }
    public Vector3 HitPosition { get; set; }
    public Vector3 HeadPosition { get; set; }
    public Vector3 HeadRotation { get; set; }
    public Vector3 EyeOrigin { get; set; }
    public Vector3 EyeDirection { get; set; }
    public Vector2 EyeHitPoint { get; set; }
    public float ResistanceChange { get; set; } 
    public int TaskRound { get; set; }  


}
